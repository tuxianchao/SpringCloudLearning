# 学习springcloud

资源：

spring cloud版本：https://repo.spring.io/libs-milestone/org/springframework/cloud/spring-cloud-dependencies/

 1. 微服务：martin fowler：https://martinfowler.com/microservices/
 2. spring 官方reference：http://cloud.spring.io/spring-cloud-static/Finchley.RELEASE/single/spring-cloud.html
 3. blog:
    
    1.http://springcloud.fun/
    
    2.http://blog.didispace.com/
    
    3.http://www.ityouknow.com/spring-cloud.html
 
 