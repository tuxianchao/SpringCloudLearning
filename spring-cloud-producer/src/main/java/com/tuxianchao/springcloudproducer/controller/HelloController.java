package com.tuxianchao.springcloudproducer.controller;

import com.tuxianchao.springcloudproducer.vo.UserVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * created by
 *
 * @author tuxianchao
 * @date 2018/6/29 下午3:47
 */
@RestController
public class HelloController {
    /**
     * 配置中心参数
     */
    @Value("${hello}")
    private String configProperties;

    @RequestMapping("/hello/{name}")
    public UserVO hello(@PathVariable("name") String name) {
        UserVO userVO = new UserVO(name, new Date());
        System.out.println("========================hello========================");
        System.out.println(userVO);
        System.out.println(configProperties);
        return userVO;
    }
}