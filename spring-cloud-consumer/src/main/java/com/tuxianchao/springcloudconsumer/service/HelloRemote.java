package com.tuxianchao.springcloudconsumer.service;

import com.tuxianchao.springcloudconsumer.vo.UserVO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * created by
 *
 * @author tuxianchao
 * @date 2018/6/29 下午4:03
 */
@FeignClient(name = "spring-cloud-producer")
public interface HelloRemote {
    /**
     * 1
     *
     * @param name
     * @return
     */


    @RequestMapping("/hello/{name}")
    public UserVO hello(@PathVariable("name") String name);
}
