package com.tuxianchao.springcloudconsumer.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * created by
 *
 * @author tuxianchao
 * @date 2018/7/1 下午10:29
 */
public class UserVO implements Serializable {

    private static final long serialVersionUID = 3935758542427682448L;

    private String name;
    private Date birth;

    public UserVO() {
    }

    public UserVO(String name, Date birth) {
        this.name = name;
        this.birth = birth;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    @Override
    public String toString() {
        return "UserVO{" +
                "name='" + name + '\'' +
                ", birth=" + birth +
                '}';
    }
}
