package com.tuxianchao.springcloudconsumer.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.tuxianchao.springcloudconsumer.service.HelloRemote;
import com.tuxianchao.springcloudconsumer.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * created by
 *
 * @author tuxianchao
 * @date 2018/6/29 下午4:04
 */
@RestController
public class TestController {


    @Autowired
    private HelloRemote helloRemote;

    /**
     * HystrixCommand熔断处理，指定熔断方法，注意熔断方法的参数要和原方法的参数一直，
     * 否则提示找不到熔断方法 fallback method wasn't found
     * <p>
     * 1.
     *
     * @param name
     * @return
     */
    @GetMapping("/hello")
    @HystrixCommand(fallbackMethod = "indexFallback")
    public Object index(@RequestParam(value = "name", defaultValue = "default") String name) {

        UserVO userVO = helloRemote.hello(name);
        System.out.println("========================hello========================");
        System.out.println(userVO);
        return userVO;
    }

    public Object indexFallback(String name) {
        return new UserVO("default", new Date());
    }

}
